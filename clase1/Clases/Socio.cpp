#include "Socio.h"
// NombreClase::NombreOperacion

// constructor x defecto
Socio::Socio() {
  this->ci = "";
  this->nombre = "";
}

// constructor con parametros
Socio::Socio(string ci, string nombre) {
  this->ci = ci;
  this->nombre = nombre;
}

// Destructor
Socio::~Socio() {}

/*
    Getters y Setters
*/
string Socio::getCi() { return this->ci; }

void Socio::setCi(string ci) { this->ci = ci; }

string Socio::getNombre() { return this->nombre; }

void Socio::setNombre(string nombre) { this->nombre = nombre; }
