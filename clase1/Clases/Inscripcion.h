#ifndef INSCRIPCION // siempre en mayusuclas las etiquetas
#define INSCRIPCION

#include "Fecha.h"
#include "Socio.h"

class Inscripcion {
private:
  Fecha fecha;
  Socio socio;

public:
  /*Constructores*/
  Inscripcion();

  Inscripcion(Fecha);

  /*Getters Setters*/
  void setFecha(Fecha);

  Fecha getFecha();

  void setSocio(Socio);

  Socio getSocio();

  /*Destructor*/
  ~Inscripcion();
};

#endif
