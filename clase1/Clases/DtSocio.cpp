#include "DtSocio.h"

// Constructores
DtSocio::DtSocio() {

  this->ci = "";
  this->nombre = "";
}

DtSocio::DtSocio(string ci, string nombre) {

  this->ci = ci;
  this->nombre = nombre;
}

// Getters
string DtSocio::getCi() { return this->ci; }

string DtSocio::getNombre() { return this->nombre; }
