#ifndef SOCIO // siempre en mayusuclas las etiquetas
#define SOCIO

#include <iostream>

using namespace std;

class Socio {
private:
  string ci;
  string nombre;

public: // operaciones para poder trabajar con ellas
  /*Constructores*/
  Socio();

  Socio(string, string);

  /*Getters Setters*/
  string getCi();

  void setCi(string);

  string getNombre();

  void setNombre(string);

  /*Destructor*/
  ~Socio();
};

#endif
