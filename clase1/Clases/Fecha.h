#ifndef FECHA // siempre en mayusuclas las etiquetas
#define FECHA

#include <string>

using namespace std;

class Fecha {
private:
  int dia;
  int mes;
  int anio;

public:
  /*Constructores*/
  Fecha(int, int, int);

  Fecha();

  /*Getters*/
  int getDia();

  int getMes();

  int getAnio();

  string getFecha();
};

#endif
