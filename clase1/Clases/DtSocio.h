#ifndef DTSOCIO
#define DTSOCIO

#include <iostream>

using namespace std;
/*
Para que sea un datatype, las operaciones no pueden modificar

el estado interno del objeto sino retornar uno nuevo

Diapositiva 3, pagina 20 y 21
*/
class DtSocio {

private:
  string ci;
  string nombre;

public:
  // Constructores
  DtSocio();

  DtSocio(string ci, string nombre);

  // Getters
  string getCi();

  string getNombre();
};

#endif
