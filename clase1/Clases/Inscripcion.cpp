#include "Inscripcion.h"
#include "Fecha.h"
#include "Socio.h"
// NombreClase::NombreOperacion

// constructor x defecto
Inscripcion::Inscripcion() {}

// constructor con parametros
Inscripcion::Inscripcion(Fecha fecha) { this->fecha = fecha; }

// destructor
Inscripcion::~Inscripcion() {}

/*
    Getters y Setters
*/
Fecha Inscripcion::getFecha() { return this->fecha; }

void Inscripcion::setFecha(Fecha fecha) { this->fecha = fecha; }

Socio Inscripcion::getSocio() { return this->socio; }

void Inscripcion::setSocio(Socio socio) { this->socio = socio; }
