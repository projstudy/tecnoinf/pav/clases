#include "Clases/Inscripcion.h"
#include "Clases/Socio.h"

int main() {

  Socio s = Socio();

  s.setNombre("mariano");

  cout << s.getNombre() << endl;

  Inscripcion i = Inscripcion();

  i.setSocio(s);

  cout << i.getSocio().getNombre() << endl;

  Fecha f = Fecha(31, 5, 1991);

  i.setFecha(f);

  cout << i.getFecha().getDia() << endl;
}
